require 'rails_helper'

RSpec.describe User, type: :model do
  before :each do
    @user = User.new
  end

  it 'generate name based on first name and second name' do
    @user.first_name = 'Arthur'
    @user.last_name = 'Dent'

    expect(@user.name).to eq('Arthur Dent')
  end
end
