class User
  include Mongoid::Document

  devise :database_authenticatable, :rememberable, :validatable

  ## Database authenticatable
  field :email, type: String
  field :encrypted_password, type: String

  ## Rememberable
  field :remember_created_at, type: Time

  field :first_name, type: String
  field :last_name, type: String

  validates :email,
            presence: true

  validates :first_name,
            presence: true

  validates :last_name,
            presence: true

  def name
    "#{first_name} #{last_name}"
  end
end
